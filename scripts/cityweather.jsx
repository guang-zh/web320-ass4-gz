"use strict";
// Guang Zhang 1942372
// JavaScript: assignment 4
import React from "react";
import ReactDOM from "react-dom";
/**
 * This program will display Weather conditions based on the City name searched, 
 * when the "Get Weather" button is clicked, 
 * the program will fetch the weather conditions from the API and display the information
 * the corresponding weather icon will be displaying.
 * It gets each weather condition by fetching from the API (https://openweathermap.org/current) 
 * 
 * input city, create a url with public key, retrieve weather from the JSON
 * use React. updates using state
 * @author Guang Zhang
 * @version 2020-12
 */

function setup() {
    // render the form
    ReactDOM.render(<CityInputForm/>, document.querySelector('#form'));
}

class CityInputForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            city: "",
            icon: "",
            showError: false,
            showResult: false,
            errorMsg: '',
            bgColour: '',
            pagenew: true
        }
        this.apikey = "5f2df38e0891b36bc6cf224924ea834f";
        this.apiURL = "https://api.openweathermap.org/data/2.5/weather?q={cityname}&appid=5f2df38e0891b36bc6cf224924ea834f";
        this.iconURL = "https://openweathermap.org/img/wn/{icon}@2x.png";
        this.icon = "";
        this.handleChange = this.change.bind(this);
        this.handleSubmit = this.submit.bind(this);
    }
    change(e) {
        if (e.target.name == "city")
            this.setState({ city: e.target.value });
        console.log(`change city ${e.target.name} to ${e.target.value}`);
    }
    submit(e) {
        e.preventDefault();
        console.log('fetch weather data ' + this.state.city);
        this.getWeatherInfo();
    }
    /**
    * read the form info & construct the url
    * then retrieve the weather data
    */
    getWeatherInfo() {
        let url = new URL(this.apiURL);
        // construct  params like  ?cityname=Montreal
        url.searchParams.set("cityname", this.state.city);
        console.log(url);
        fetch(url)
            .then(response => {
                if (!response.ok) {
                    throw new Error(`HTTP error! status: ${response.status}`);
                } else if (response.status == 404) {
                    throw new Error(`City name is not found: ${response.status}`);
                } 
                else {
                    return response.json();
                }
            }).then(json => this.dataRender(json))
            .catch(e => { this.errorRender(e, url) });
    }
     getWeatherImg() {
        let url = new URL(this.iconURL);
        // construct  params like  ?cityname=Montreal
        url.searchParams.set("icon", this.state.city);
        console.log(url);
        fetch(url)
            .then(response => {
                if (!response.ok) {
                    throw new Error(`HTTP error! status: ${response.status}`);
                } else {
                    return response.json();
                }
            }).then(json => this.dataRender(json))
            .catch(e => { this.errorRender(e, url) });
    }
    errorRender(e, url) {
        console.log("Error "+e.message +  " ")
        console.log("url   " + url)
        this.setState({ showError: true, showResult: false, errorMsg: e.message, 
            bgColour: "red"})
    }
    /**
     * given JSOn with  weather in text
     * put  it  into the state for the page
     * 
     * @param {JSON} jsondata 
     */
    dataRender(jsondata) {
        console.log(jsondata);
        this.setState({ showError: false, showResult: true, })
        this.setState({ weather: jsondata.weather['description'],
            bgColour: "darkorange"} )
    }
    render() {
         const styleResult = { backgroundColor: this.state.bgColour ,
                 display: this.state.showResult ? "block" : "none",}
        const styleError = { backgroundColor: this.state.bgColour ,
                    display: this.state.showError ? "block" : "none",}
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <fieldset id="cityInput">
                        <legend>Search City for Weather Conditions</legend>
                        <label htmlFor="city">City: </label>
                        <input type="text" id="city" name="city" required value={this.state.city}
                            onChange={this.handleChange} />
                    </fieldset>
                    <input type="submit" id="submit" value="Get Weather"></input>
                </form>
                <div id='weatherInfo' style={ styleResult } >
                    <p >Weather: {this.state.weather['main']} </p>
                </div>
                <div id='error' style={ styleError }>
                    <p >Error: {this.state.errorMsg} </p>
                </div>
                <div id='weatherImg' style = {styleResult}>
                    <img>Weather Icon: {this.state.icon}</img>
                </div>
                
            </div>
        );
    }
}

setup()