"use strict";
// Guang Zhang 1942372
// JavaScript: assignment 4

/**
 * This program will display Weather conditions based on the City name searched, 
 * when the "Get Weather" button is clicked, 
 * the program will fetch the weather conditions from the API and display the information
 * the corresponding weather icon will be displaying.
 * It gets each weather condition by fetching from the API (https://openweathermap.org/current) 
 * @auhtor Guang Zhang
 */

document.addEventListener("DOMContentLoaded", setup);

let global = {}; //global namespace

/**
 * This function declares global variables  
 * when submit button is clicked, fetch weather conditions from API
 * display weather info and change weather image.
 */
function setup() {
    global.apiURL = "https://openweathermap.org/weather-conditions";
    global.apikey = "5f2df38e0891b36bc6cf224924ea834f";
    global.city=document.querySelector("#city");
    console.log(`City name is ${global.city}`);
    global.section=document.querySelector("#weather");
    document.querySelector('form').addEventListener('submit',getWeather);
    global.btn= document.querySelector('#btn');
    global.btn.addEventListener('click', changeImage);
    global.img = document.querySelector("#weatherImg");
}


/**
 * Read the form Search city item and construct the url 
 * Retrieve the Weather info from the API
 * @param {*} e 
 */
function getWeather(e) {
    e.preventDefault();
    let url = constructURL();
    getJSON(url, displayWeather);
}


/**
 * Construct the URL for retrieving from the API with the search city value
 */
function constructURL() {
    let url = new URL(globals.apiURL);
    // construct params querystring ?search=value
    url.searchParams.set("search", globals.city.value);
    console.log(url);
    return url;
}

/**
 * Given JSON data with the search city name in text
 * Create p element and associtate with the JSON data as its textContent
 * Add the p element to the section in the html document
 * @param {JSON} jsondata
 */
function displayWeather(jsondata) {
    console.log(jsondata);
    let p = document.createElement('p');
    p.textContent=`Weather conditions`;
    global.section.appendChild(p);    
}


/**
 * Retrieve json from the weather api and perform action on it
 * Display error message if the search city item is not supported by the API
 * @param {string} uri 
 * @param {function to read JSON} action 
 */
function getJSON(uri, action) {
    fetch(uri)
        .then(response => {
            if (!response.ok){
                throw new Error(`status: ${response.status}`);
            } else {
                return response.json();
            }
        }).then(json => action(json))
            .catch (e =>
                { console.log('Error: city name not supported by the API.');
                    console.log(e.message+" "+uri);
                });
}


/**
 * Change Image based on the Weather conditions
 */
function changeImage(){
    e.preventDefault();
    document.querySelector("#weatherImg").src = "../images/10d@2x.png";
}